/*! @anamo/ember-coldstart v1.0.0 | © 2006-present, Anamo Inc. MIT License | bitbucket.org/anamo/ember-coldstart */

import Mixin from '@ember/object/mixin'
import config from 'ember-get-config'
import {
	on
} from '@ember/object/evented'
import {
	defer,
	hashSettled
} from 'rsvp'
import EmberObject from '@ember/object'
import Evented from '@ember/object/evented'

export default Mixin.create({
	createColdstart: on('init', function() {
		let voley = {},
			promises = (config.COLDSTART.keys || '').split(' ') || []

		promises.forEach(item => {
			voley[item] = defer()
		}, this)


		this.
		set('COLDSTART', EmberObject.extend(Evented, {
			coldstart() {}
		}).create(voley))

		hashSettled(promises.map(item => {
			return this.get(`COLDSTART.${item}.promise`)
		}, this)).
		then(hash => {
			this.get('COLDSTART').trigger('coldstart', hash)
		})
	}),
})
