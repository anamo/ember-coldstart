'use strict';

const COLDSTART_CONFIG = require('../../../../config/coldstart')

module.exports = function(/* environment, appConfig */) {
  let ENV = {
		COLDSTART: COLDSTART_CONFIG
	};
	return ENV;
};
